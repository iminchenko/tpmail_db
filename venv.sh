#!/usr/bin/env bash

# virtualenv -p pypy3 venv
virtualenv -p python3.6 venv

source venv/bin/activate

pip install --upgrade pip

pip install six
pip install -r requirements.txt