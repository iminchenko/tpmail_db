from aiohttp import web
import asyncio
import asyncpg
import json
from datetime import datetime, timedelta
from tables_creation import CONNECTION_DICT

import uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


def threadSlugFilter(slug):
    if slug.isdigit():
        return "id=%d" % int(slug)
    else:
        return "LOWER(slug) = LOWER('%s')" % slug


def getForumDict(forum_id, slug, title, forum_user_nickname, posts_count, threads_count):
    return { 
        "posts" : posts_count,
         "slug" : slug, 
         "threads" : threads_count, 
         "title" : title, 
         "user" : forum_user_nickname 
        }


def getThreadDict(thread_id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value):
    return {
        "author" : user_nickname,
        "forum" : forum_name,
        "id" : thread_id,
        "message" : message,
        "title" : title,
        "created" : created,
        "slug" : slug,
        "votes" : vote_value
    }


def getUserDict(nickname, email, about, fullname):
    return {
        "nickname" : nickname,
        "email" : email,
        "about" : about,
        "fullname" : fullname
    }


def getPostDict(user_nickname, created, forum_name, id, message, thread_id, parent_id, isedited):
    return {
        "author" : user_nickname,
        "created" : created,
        "forum" : forum_name,
        "id" : id,
        "message" : message,
        "thread" : thread_id,
        "parent" : parent_id,
        "isEdited" : isedited
        }


def convertDateTime(time):
    DATETIME_FORMAT2 = "%Y-%m-%dT%H:%M:%S.%f"

    dt = datetime.strptime(time[0:23], DATETIME_FORMAT2)
    dt = dt - timedelta(hours = int(time[23:26]))

    return dt.strftime(DATETIME_FORMAT2)[0:23] + "Z"



async def nothing(request):
    return web.json_response(data=None,status = 200)


async def userSlugCreate(request):
    slug = request.match_info.get('slug')
    request_json = await request.json()
    about = request_json["about"]
    email = request_json["email"]
    fullname = request_json["fullname"]
    nickname = slug

    pool = request.app['pool']
    async with pool.acquire() as conn:
        conflicts_result = await conn.fetch("SELECT nickname, email, about, fullname FROM users WHERE LOWER(nickname) = LOWER('%s') OR LOWER(email) = LOWER('%s')" % (nickname, email))
    conflicts = []

    for i in conflicts_result:
        conflicts.append(getUserDict(*i))

    if conflicts:
        return web.json_response(text=json.dumps(conflicts), status = 409)

    # Creating user
    async with pool.acquire() as conn:
        await conn.execute("INSERT INTO users (nickname, email, about, fullname, nick_ordering) VALUES ('%s', '%s', '%s', '%s', lower(replace(replace('%s', '_', '1'), '.', '0')))" % (nickname, email, about, fullname, nickname))

    return web.json_response(data=getUserDict(nickname, email, about, fullname), status=201)


async def userSlugProfileGet(request):
    slug = request.match_info.get('slug')
    pool = request.app['user_profile_pool']
    
    async with pool.acquire() as conn:
        user_result = await conn.fetchval("EXECUTE user_profile ('%s');" % slug)

    # pool = request.app['pool']
    
    # async with pool.acquire() as conn:
    #     user_result = await conn.fetchval("SELECT nickname, email, about, fullname FROM users WHERE LOWER(nickname) = LOWER('%s') FETCH FIRST ROW ONLY;" % slug)
    

    if not user_result:
        return web.json_response(data = { "message": "Can't find user with id %s" % slug }, status=404)

    return web.Response(text = user_result, status = 200, content_type="application/json")


async def userSlugProfile(request):
    slug = request.match_info.get('slug')
    pool = request.app['pool']
    
    async with pool.acquire() as conn:
        user_result = await conn.fetchrow("SELECT id, nickname, email, about, fullname FROM users WHERE LOWER(nickname) = LOWER('%s') FETCH FIRST ROW ONLY;" % slug)
    
    if not user_result:
        return web.json_response(data = { "message": "Can't find user with id %s" % slug }, status=404)

    user_id, user_nickname, user_email, user_about, user_fullname = user_result

    if request.method == 'GET':
        return web.json_response(data={"about" : user_about,
                                            "email" : user_email,
                                            "fullname" : user_fullname,
                                            "nickname" : user_nickname}, status=200)
    else:
        request_json = await request.json()
        if "email" in request_json.keys():
            email = request_json["email"]
            async with pool.acquire() as conn:
                conflict = await conn.fetchrow("SELECT nickname FROM users WHERE LOWER(email) = LOWER('%s') FETCH FIRST ROW ONLY;" % email)
            if conflict:
                return web.json_response(data = {"message" : "This email is already registered by user: %s" % conflict}, status=409)

            user_email = email

        if "about" in request_json.keys():
            about = request_json["about"]
            user_about = about

        if "fullname" in request_json.keys():
            fullname = request_json["fullname"]
            user_fullname = fullname

        async with pool.acquire() as conn:
            await conn.execute("UPDATE users SET email='%s', about='%s', fullname='%s' WHERE id=%d;" % (user_email, user_about, user_fullname, user_id))

        return web.json_response(data = {"about" : user_about, "email" : user_email,
                    "fullname" : user_fullname, "nickname" : user_nickname}, status = 200)


async def forumCreate(request):
    request_json = await request.json()

    slug = request_json["slug"]
    title = request_json["title"]
    nickname = request_json["user"]

    pool = request.app['pool']
    async with pool.acquire() as conn:
        forum_result = await conn.fetchrow("SELECT id, title, name, user_nickname, posts_count, threads_count FROM forums WHERE LOWER(name) = LOWER('%s') FETCH FIRST ROW ONLY;" % slug)

    if forum_result:
        forum_id, title, slug, forum_user_nickname, posts_count, threads_count = forum_result
        return web.json_response(data=getForumDict(forum_id, slug, title, forum_user_nickname, posts_count, threads_count), status=409)

    async with pool.acquire() as conn:
        user_result = await conn.fetchrow("SELECT id, nickname FROM users WHERE LOWER(nickname) = LOWER('%s') FETCH FIRST ROW ONLY;" % nickname)

    if not user_result:
        return web.json_response(data={"message" : "Can't find user with nickname: %s" % nickname }, status=404)

    user_id, nickname = user_result

    async with pool.acquire() as conn:
        forum_id = await conn.fetchval("""INSERT INTO forums (title, user_id, name, user_nickname, threads_count, posts_count)
                                    VALUES ('%s', %d, '%s', '%s', 0, 0) RETURNING id;""" % (title, user_id, slug, nickname))

    return web.json_response(data= getForumDict(forum_id, slug, title, nickname, 0, 0), status=201)


async def forumSlugDetails(request):
    slug = request.match_info.get('slug')

    # pool = request.app['pool']
    # async with pool.acquire() as conn:
    #     forum_result = await conn.fetchval("SELECT row_to_json(r) FROM (SELECT id, name as slug, title, user_nickname as user, posts_count as posts, threads_count as threads FROM forums WHERE LOWER(name) = LOWER('%s') FETCH FIRST ROW ONLY) as r" % slug)

    pool = request.app['forum_details_pool']
    async with pool.acquire() as conn:
        forum_result = await conn.fetchval("EXECUTE forum_details('%s')" % slug)

    if not forum_result:
        return web.json_response(data={"message": "Can't find forum with slug: %s" % slug}, status=404)

    return web.Response(text = forum_result, status = 200, content_type="application/json")


async def forumSlugCreate(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    request_json = await request.json()

    author = request_json["author"]

    message = request_json["message"]
    title = request_json["title"]

    async with pool.acquire() as conn:
        forum_result = await conn.fetchrow("SELECT id, name FROM forums WHERE LOWER(name) = LOWER('%s') FETCH FIRST ROW ONLY;" % (slug))

    if not forum_result:
        return web.json_response(data={ "message": "Can't find forum by slug: %s" % slug }, status=404)

    forum_id, forum_name = forum_result

    async with pool.acquire() as conn:
        user_result = await conn.fetchrow("SELECT id, nickname, fullname, email, about, nick_ordering FROM users WHERE LOWER(nickname) = LOWER('%s') FETCH FIRST ROW ONLY;" % (author))

    if not user_result:
        return web.json_response(data={ "message" : "Can't find thread author by nickname: %s" % author }, status=404)

    user_id, user_nickname, user_fullname, user_email, user_about, nick_ordering = user_result

    insertion = ""
    values = ""

    threadDict = {
        "author" : author,
        "forum" : forum_name,
        "message" : message,
        "title" : title
    }

    if "slug" in request_json.keys():
        async with pool.acquire() as conn:
            collision_result = await conn.fetchrow("SELECT id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value FROM threads WHERE LOWER(slug) = LOWER('%s') FETCH FIRST ROW ONLY;" % (request_json["slug"]))

        if collision_result:
            return web.json_response(data=getThreadDict(*collision_result), status=409)

        values = values + "'" + request_json["slug"] + "', "
        insertion = insertion + "slug, "
        threadDict["slug"] = request_json["slug"]
    if "created" in request_json.keys():
        created = convertDateTime(request_json["created"])

        insertion = insertion + "created, "
        values = values + "'" + created + "', "
        threadDict["created"] = created

    insertion = insertion + "user_id, forum_id, title, message, user_nickname, forum_name, vote_value"
    values = values + "%d, %d, '%s', '%s', '%s', '%s', 0" % (user_id, forum_id, title, message, user_nickname, forum_name)

    async with pool.acquire() as conn:
        id = await conn.fetchval("INSERT INTO threads(" + insertion + ") VALUES (" + values + ") RETURNING id;")
        await conn.execute("UPDATE forums SET threads_count=(SELECT COUNT(*) FROM threads WHERE forum_id=%d) WHERE id=%d;" % (forum_id, forum_id))
                
        await conn.execute("""
                        INSERT INTO userforum(forum_id, nickname, email, about, fullname, nick_ordering)
                        VALUES (%d, '%s', '%s', '%s', '%s', '%s') ON CONFLICT DO NOTHING;
                        """ % (forum_id, author, user_email, user_about, user_fullname, nick_ordering))

    threadDict["id"] = id

    return web.json_response(data = threadDict, status=201)


async def forumSlugThreads(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        forum_id = await conn.fetchval("SELECT id FROM forums WHERE LOWER(name) = LOWER('%s') FETCH FIRST ROW ONLY;" % slug)

        if not forum_id:
            return web.json_response(data={ "message": "Can't find forum by slug: %s" % slug }, status=404)

        request_args = request.query

        if "desc" in request_args.keys() and request_args.get("desc") == "true":
            ordering = "ORDER BY created DESC"
            desc = True
        else:
            ordering = "ORDER BY created ASC"
            desc = False

        limit = ""

        if "limit" in request_args.keys():
            limit = "LIMIT " + request_args.get("limit")

        filtering = ""
        if request_args.get("since"):
            sincedate = request_args.get("since")

            if (len(sincedate) > 24):
                sincedate = convertDateTime(sincedate)

            filtering = "AND created %s '%s'" % ("<=" if desc else ">=", sincedate)

        threads = await conn.fetchval("SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT threads.id, created, title, message, slug, user_nickname as author, forum_name as forum, vote_value as votes FROM threads INNER JOIN (SELECT id FROM threads WHERE forum_id=%d %s %s %s) as foo ON threads.id=foo.id %s) as r;" % (forum_id, filtering, ordering, limit, ordering))

    return web.Response(text = threads if threads else "[]", status = 200, content_type="application/json")


async def threadSlugCreate(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    created = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    
    async with pool.acquire() as conn:    
        thread_result = await conn.fetchrow("SELECT id, forum_id, forum_name FROM threads WHERE " + threadSlugFilter(slug) + " FETCH FIRST ROW ONLY;")
                
        if not thread_result:
            return web.json_response(data={"message" : "Can't find post thread by id %s" % slug}, status=404)

        thread_id, thread_forum_id, thread_forum_name = thread_result

        request_json = await request.json()

        if not request_json:
            return web.json_response(data=[],
                                    status=201)

        post_dicts = []
        post_ids=[]

        author = ""

        prepared = await conn.prepare("INSERT INTO posts (message, forum_id, thread_id, user_id, created, parent_id, user_nickname, forum_name, user_email, user_about, user_fullname, tree_parent_path, root) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING id, forum_id;")

        for post_json in request_json:
            if (author !=  post_json["author"]):
                author = post_json["author"]

                user_result = await conn.fetchrow("SELECT id, nickname, email, about, fullname, nick_ordering FROM users WHERE LOWER(nickname) = LOWER('%s') FETCH FIRST ROW ONLY;" % author)
                if not user_result:
                    return web.json_response(data={"message": "Can't find post author by nickname: %s" % author}, status=404)
                user_id, user_nickname, user_email, user_about, user_fullname, nick_ordering = user_result

                await conn.execute("""
                                INSERT INTO userforum(forum_id, nickname, email, about, fullname, nick_ordering)
                                VALUES (%d, '%s', '%s', '%s', '%s', '%s') ON CONFLICT DO NOTHING;
                                """ % (thread_forum_id, author, user_email, user_about, user_fullname, nick_ordering))

            message = post_json["message"]

            post_dict = {
                "author" : user_nickname,
                "created" : created,
                "forum" : thread_forum_name,
                "message" : message,
                "thread" : thread_id
            }

            if "parent" in post_json.keys():
                parent_id = post_json["parent"]

                parent_result = await conn.fetchrow("SELECT thread_id, tree_path, root FROM posts WHERE id=%d FETCH FIRST ROW ONLY;" % parent_id)

                if (not parent_result) or not parent_result[0] == thread_id:
                    return web.json_response(data={"message": "Parent post was created in another thread"}, status=409)

                post_dict["parent"] = parent_id
                parent_path = parent_result[1]
                if not parent_path:
                    parent_path = []
                root = int(parent_result[2])
            else:
                parent_id = None
                parent_path = []
                root = None

            id_forum_id = await prepared.fetchrow(message, thread_forum_id, thread_id, user_id, created, parent_id, user_nickname, thread_forum_name, user_email, user_about, user_fullname, parent_path, root)

            # await conn.execute("UPDATE posts SET root=%d, tree_path=array_append(tree_path, id) WHERE id=%d" % (id_forum_id[0] if root is None else root, id_forum_id[0]))

            if parent_id is None:
                await conn.execute("UPDATE posts SET root=%d WHERE id=%d" % (id_forum_id[0], id_forum_id[0]))

            post_ids.append(id_forum_id)
            
            post_dicts.append(post_dict)

        dict_iter = iter(post_dicts)

        forum_ids = dict()

        for i in post_ids:
            next(dict_iter)["id"] = i[0]
            if i[1] in forum_ids.keys():
                forum_ids[i[1]] = forum_ids[i[1]] + 1
            else:
                forum_ids[i[1]] = 1

        for key, value in forum_ids.items():
            await conn.execute("UPDATE forums SET posts_count=posts_count+%d WHERE id=%d;" % (value, key))

    return web.json_response(data=post_dicts,
                                status= 201)


async def threadSlugVote(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        thread_result = await conn.fetchrow("SELECT id, forum_id, created, title, message, slug, user_nickname, forum_name FROM threads WHERE " + threadSlugFilter(slug) + " FETCH FIRST ROW ONLY;")

        if thread_result is None:
            return web.json_response(data = {"message" : "Can't find post thread by id %s" % slug}, status=404)

        request_json = await request.json()

        nickname = request_json["nickname"]

        user_id = await conn.fetchval("SELECT id FROM users WHERE nickname='%s';" % nickname)
        if not user_id:
            return web.json_response(data={"message" : "Can't find user with nickname: %s" % nickname }, status=404)

        thread_id, forum_id, created, title, message, thread_slug, author_nickname, forum_name = thread_result

        prev_vote = await conn.fetchval("SELECT voice FROM votes WHERE thread_id=%d AND user_id=%d" % (thread_id, user_id))

        if prev_vote:
            await conn.execute(""" UPDATE votes
                                SET voice=%d
                                WHERE thread_id=%d AND user_id=%d
                            """ % (request_json["voice"], thread_id, user_id))
        else:
            await conn.execute("""
                INSERT INTO votes(voice, thread_id, user_id) VALUES
                (%d, %d, %d);
            """ % (request_json["voice"], thread_id, user_id))

        vote_value = await conn.fetchval("UPDATE threads SET vote_value=(SELECT SUM(voice) FROM votes WHERE thread_id=%d) WHERE id=%d RETURNING vote_value;" % (thread_id, thread_id))

    return web.json_response(data = getThreadDict(thread_id, forum_id, created, title, message, thread_slug, author_nickname, forum_name, vote_value), status = 200)


async def threadSlugDetailsGet(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        thread_result = await conn.fetchval("SELECT row_to_json(r) FROM (SELECT id, created, title, message, slug, user_nickname as author, forum_name as forum, vote_value as votes FROM threads WHERE %s FETCH FIRST ROW ONLY) as r;" %  threadSlugFilter(slug))

    if not thread_result:
        return web.json_response(data={"message" : "Can't find post thread by id %s" % slug}, status=404)

    return web.Response(text = thread_result, status = 200, content_type="application/json")


async def threadSlugDetails(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        thread_result = await conn.fetchrow("SELECT id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value FROM threads WHERE %s FETCH FIRST ROW ONLY;" %  threadSlugFilter(slug))

    if not thread_result:
        return web.json_response(data={"message" : "Can't find post thread by id %s" % slug}, status=404)

    thread_id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value = thread_result

    if request.method == 'POST':
        params = await request.json()

        changed = False

        if "title" in params.keys() and not (title == params["title"]):
            changed = True
            title = params["title"]
        if "message" in params.keys() and not (message == params["message"]):
            changed = True
            message = params["message"]

        if changed:
            async with pool.acquire() as conn:
                await conn.execute("UPDATE threads SET message='%s', title='%s' WHERE id=%d" % (message, title, thread_id))

    return web.json_response(data=getThreadDict(thread_id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value), status=200)


async def threadSlugPosts(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        thread_id = await conn.fetchval("SELECT id FROM threads WHERE %s FETCH FIRST ROW ONLY;" % threadSlugFilter(slug))
        
    if not thread_id:
        return web.json_response(data={"message" : "Can't find post thread by id %s" % slug}, status=404)

    if "since" in request.query:
        since = int(request.query.get("since"))
    else:
        since = None

    if "sort" in request.query:
        sort = request.query.get("sort")
    else:
        sort = "flat"

    if "desc" in request.query.keys():
        desc = request.query.get("desc") == "true"
    else:
        desc = False

    limit = ""
    if "limit" in request.query:
        limit = "LIMIT " + request.query.get("limit")
    if desc:
        ordering = "DESC"
    else:
        ordering = "ASC"
    ordering = "ORDER BY created %s, id %s" % (ordering, ordering)

    async with pool.acquire() as conn:
        if sort == "flat":
            offset = 0
            wheresince = ""
            if not (since is None):
                if desc:
                    wheresince = "AND id < %s" % since
                else:
                    wheresince = "AND id > %s" % since

            posts = await conn.fetchval("SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT user_nickname as author, created, forum_name as forum, posts.id, message, thread_id as thread, parent_id as parent, isedited as isEdited FROM posts INNER JOIN (SELECT id FROM posts WHERE thread_id=%d %s %s %s) as foo ON posts.id=foo.id %s) as r;" % (thread_id, wheresince, ordering, limit, ordering))

        elif sort == "tree":
            offset = 0
            sincewhere = ""
            if not (since is None):
                since =  await conn.fetchval("SELECT tree_path FROM posts WHERE id=%d;" % since)

                sincewhere = "AND tree_path %s ARRAY%s" % ("<" if desc else ">", str(since))
                # offset = await conn.fetchval(
                # """ WITH RECURSIVE tree (path, id) as (
                #     SELECT array[CAST(row_number() OVER (ORDER BY created ASC, id ASC) AS INT)], id
                #     FROM posts
                #     WHERE thread_id=%d AND parent_id IS NULL
                #     UNION ALL
                #     SELECT array_append(path, CAST(row_number() OVER (ORDER BY posts.created ASC, posts.id ASC) AS INT)), posts.id
                #     FROM posts
                #         INNER JOIN tree ON tree.id = posts.parent_id
                #     )
                #     SELECT row FROM (
                #     SELECT id, row_number() OVER(ORDER BY path %s) as row
                #     FROM tree) as foo WHERE id=%d;
                #     """ % (thread_id, "DESC" if desc else "ASC", since)
                # )

            posts = await conn.fetchval("""
                SELECT array_to_json(array_agg(row_to_json(r))) FROM 
                (SELECT user_nickname as author, created, forum_name as forum, posts.id, message, thread_id as thread, parent_id as parent, isedited as isEdited
                FROM posts WHERE thread_id=%d %s ORDER BY tree_path %s %s) as r;
            """ % (thread_id, sincewhere, "DESC" if desc else "ASC", limit))

            # posts = await conn.fetchval(
            #     """ WITH RECURSIVE tree (path, id, user_nickname, created, forum_name, message, thread_id, parent_id, isedited) as (
            #         SELECT array[CAST(row_number() OVER (ORDER BY created ASC, id ASC) AS INT)], id, user_nickname, created, forum_name, message, thread_id, parent_id, isedited
            #         FROM posts
            #         WHERE thread_id=%d AND parent_id IS NULL
            #         UNION ALL
            #         SELECT array_append(path, CAST(row_number() OVER (ORDER BY posts.created ASC, posts.id ASC) AS INT)), posts.id, posts.user_nickname, posts.created, posts.forum_name, posts.message, posts.thread_id, posts.parent_id, posts.isedited
            #         FROM posts
            #             INNER JOIN tree ON tree.id = posts.parent_id
            #         )
            #         SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT user_nickname as author, created, forum_name as forum, id, message, thread_id as thread, parent_id as parent, isedited as isEdited
            #         FROM tree
            #         ORDER BY path %s OFFSET %d %s) as r;""" % (thread_id, "DESC" if desc else "ASC", offset, limit)
            # )

        elif sort == "parent_tree":
            offset = 0
            wheresince = ""
            if not (since is None):
                since =  await conn.fetchval("SELECT root FROM posts WHERE id=%s FETCH FIRST ROW ONLY;" % since)
                
                if desc:
                    wheresince = "AND id < %d" % since
                else:
                    wheresince = "AND id > %d" % since

            posts = await conn.fetchval("""
                SELECT array_to_json(array_agg(row_to_json(r))) FROM 
                (SELECT user_nickname as author, created, forum_name as forum, posts.id, message, thread_id as thread, parent_id as parent, isedited as isEdited FROM posts 
                WHERE root IN (SELECT id FROM posts WHERE thread_id=%d AND parent_id is null %s %s %s) ORDER BY root %s, tree_path ASC, created ASC) as r
                """ % (thread_id, wheresince, ordering, limit, "DESC" if desc else "ASC"))

            # posts = await conn.fetchval(
            #     """ WITH RECURSIVE tree (path, id, user_nickname, created, forum_name, message, thread_id, parent_id, isedited) as (
            #         SELECT array[CAST(row_number() OVER (%s) AS INT)], id, user_nickname, created, forum_name, message, thread_id, parent_id, isedited
            #         FROM (SELECT * FROM posts
            #         WHERE thread_id=%d AND parent_id IS NULL %s %s %s) as foo
            #         UNION ALL
            #         SELECT array_append(path, CAST(row_number() OVER (ORDER BY posts.created ASC, posts.id ASC) AS INT)), posts.id, posts.user_nickname, posts.created, posts.forum_name, posts.message, posts.thread_id, posts.parent_id, posts.isedited
            #         FROM posts
            #             INNER JOIN tree ON tree.id = posts.parent_id
            #         )
            #         SELECT array_to_json(array_agg(row_to_json(r))) FROM 
            #         (SELECT user_nickname as author, created, forum_name as forum, id, message, thread_id as thread, parent_id as parent, isedited as isEdited
            #         FROM tree
            #         ORDER BY path ASC) as r;""" % (ordering, thread_id, wheresince, ordering, limit)
            # )

    return web.Response(text = posts if posts else "[]", status = 200, content_type="application/json")


async def forumSlugUsers(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    async with pool.acquire() as conn:
        forum_id = await conn.fetchval("SELECT id FROM forums WHERE LOWER(name) = LOWER('%s') FETCH FIRST ROW ONLY;" % slug)

    if not forum_id:
        return web.json_response(data={"message" : "Can't find forum by slug: %s" % slug}, status=404)

    desc = request.query.get("desc") == "true"
    if desc:
        ordering = "ORDER BY nick_ordering DESC"
    else:
        ordering = "ORDER BY nick_ordering ASC"

    sincewhere = ""
    if "since" in request.query:
        sincewhere = "AND nick_ordering %s lower(replace(replace('%s', '_', '1'), '.', '0'))" % ("<" if desc else ">", request.query.get("since"))

    limit = ""
    if "limit" in request.query:
        limit = "LIMIT " + request.query.get("limit")

    async with pool.acquire() as conn:
        users = await conn.fetchval("SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT nickname, email, about, fullname FROM userforum WHERE forum_id=%d %s %s %s) as r;" % (forum_id, sincewhere, ordering, limit))
        
    # return web.json_response(data = [getUserDict(*user) for user in users], status = 200)
    return web.Response(text = users if users else "[]", status = 200, content_type="application/json")


async def postSlugDetailsGet(request):
    slug = request.match_info.get('slug')

    # pool = request.app['pool']
    pool = request.app['post_details_pool']

    user = 0
    forum = 0
    thread = 0
    if "related" in request.query.keys():
        related = request.query.get("related")
        if "user" in related:
            user = 1
        if "forum" in related:
            forum = 1
        if "thread" in related:
            thread = 1

    async with pool.acquire() as conn:
        # post_result = await conn.fetchval("""
        #     SELECT row_to_json(r) FROM (SELECT (SELECT row_to_json(r) FROM (SELECT posts1.user_nickname as author, posts1.created, posts1.forum_name as forum, posts1.id, posts1.message, posts1.thread_id as thread, posts1.parent_id as parent, posts1.isedited as "isEdited") as r) as post,
        #     CASE WHEN 1=%d THEN (SELECT row_to_json(r) FROM (SELECT posts1.user_nickname as nickname, posts1.user_email as email, posts1.user_about as about, posts1.user_fullname as fullname) as r) ELSE null END as author,
        #     CASE WHEN 1=%d THEN (SELECT row_to_json(r) FROM (SELECT posts_count as posts, name as slug, threads_count as threads, title, user_nickname as user FROM forums WHERE id=posts1.forum_id) as r) ELSE null END as forum,
        #     CASE WHEN 1=%d THEN (SELECT row_to_json(r) FROM (SELECT id, created, title, message, slug, user_nickname as author, forum_name as forum, vote_value as votes FROM threads WHERE id=posts1.thread_id) as r) ELSE null END as thread
        #     FROM
        #     (SELECT * FROM posts
        #     WHERE id=%d FETCH FIRST ROW ONLY) as posts1) as r
        # """ % (user, forum, thread, int(slug)))
        post_result = await conn.fetchval("EXECUTE post_details(%d, %d, %d, '%d')" % (user, forum, thread, int(slug)))

    if not post_result:
        return web.json_response(data={"message" : "Can't find post with id: %s" % slug}, status=404)

    return web.Response(text = post_result, status = 200, content_type="application/json")


async def postSlugDetails(request):
    slug = request.match_info.get('slug')

    pool = request.app['pool']

    info = dict()

    async with pool.acquire() as conn:
        post_result = await conn.fetchrow("SELECT id, forum_id, thread_id, parent_id, message, created, isedited, user_nickname, forum_name, user_email, user_about, user_fullname FROM posts WHERE id=%d FETCH FIRST ROW ONLY;" % int(slug))

        if not post_result:
            return web.json_response(data={"message" : "Can't find post with id: %s" % slug}, status=404)

        post_id, forum_id, thread_id, parent_id, message, created, isedited, user_nickname, forum_name, user_email, user_about, user_fullname = post_result

        if "related" in request.query.keys():
            related = request.query.get("related")
            if "user" in related:
                info["author"] = getUserDict(user_nickname, user_email, user_about, user_fullname)
            if "thread" in related:
                thread_result = await conn.fetchrow("SELECT id, forum_id, created, title, message, slug, user_nickname, forum_name, vote_value FROM threads WHERE id=%d;" % thread_id)
                info["thread"] = getThreadDict(*thread_result)
            if "forum" in related:
                forum_result = await conn.fetchrow("SELECT id, name, title, user_nickname, posts_count, threads_count FROM forums WHERE id=%d;" % forum_id)
                info["forum"] = getForumDict(*forum_result)

        if request.method == 'POST':
            params = await request.json()

            if "message" in params.keys() and not (message == params["message"]):
                message = params["message"]
                isedited = True

                await conn.execute("UPDATE posts SET message='%s', isedited=true WHERE id=%d;" % (message, post_id))

            return web.json_response(data=getPostDict(user_nickname, created, forum_name, post_id, message, thread_id, parent_id, isedited), status=200)

    info["post"] = getPostDict(user_nickname, created, forum_name, post_id, message, thread_id, parent_id, isedited)

    return web.json_response(data=info, status=200)


async def status(request):
    pool = request.app['pool']

    async with pool.acquire() as conn:
        return web.json_response(data={
            "forum" : await conn.fetchval("SELECT COUNT (*) FROM forums"),
            "post" : await conn.fetchval("SELECT COUNT (*) FROM posts"),
            "thread" : await conn.fetchval("SELECT COUNT (*) FROM threads"),
            "user" : await conn.fetchval("SELECT COUNT (*) FROM users")},
            status=200)


from migrate import migrate
async def clear(request):
    await migrate()
    return web.json_response(data=None, status=200)


async def init_user_profile_pool(conn):
    await conn.execute("""
                PREPARE user_profile (text) as SELECT row_to_json(r) FROM (SELECT nickname, email, about, fullname FROM users WHERE LOWER(nickname) = LOWER($1) FETCH FIRST ROW ONLY) as r;
                """)


async def init_forum_details_pool(conn):
    await conn.execute("""
                PREPARE forum_details(text) as SELECT row_to_json(r) FROM (SELECT id, name as slug, title, user_nickname as user, posts_count as posts, threads_count as threads FROM forums WHERE LOWER(name) = LOWER($1) FETCH FIRST ROW ONLY) as r;
                """)


async def init_post_details_pool(conn):
    await conn.execute("""
            PREPARE post_details(integer, integer, integer, integer) as
            SELECT row_to_json(r) FROM (SELECT (SELECT row_to_json(r) FROM (SELECT posts1.user_nickname as author, posts1.created, posts1.forum_name as forum, posts1.id, posts1.message, posts1.thread_id as thread, posts1.parent_id as parent, posts1.isedited as "isEdited") as r) as post,
            CASE WHEN 1=$1 THEN (SELECT row_to_json(r) FROM (SELECT posts1.user_nickname as nickname, posts1.user_email as email, posts1.user_about as about, posts1.user_fullname as fullname) as r) ELSE null END as author,
            CASE WHEN 1=$2 THEN (SELECT row_to_json(r) FROM (SELECT posts_count as posts, name as slug, threads_count as threads, title, user_nickname as user FROM forums WHERE id=posts1.forum_id) as r) ELSE null END as forum,
            CASE WHEN 1=$3 THEN (SELECT row_to_json(r) FROM (SELECT id, created, title, message, slug, user_nickname as author, forum_name as forum, vote_value as votes FROM threads WHERE id=posts1.thread_id) as r) ELSE null END as thread
            FROM
            (SELECT * FROM posts
            WHERE id=$4 FETCH FIRST ROW ONLY) as posts1) as r
            """)

async def my_web_app():
    app = web.Application()
    app.add_routes([
                        web.get('/api', nothing),

                        web.get('/api/user/{slug}/profile', userSlugProfileGet),
                        web.get('/api/forum/{slug}/details', forumSlugDetails),
                        web.get('/api/forum/{slug}/threads', forumSlugThreads),
                        web.get('/api/thread/{slug}/details', threadSlugDetailsGet),
                        web.get('/api/thread/{slug}/posts', threadSlugPosts),
                        web.get('/api/forum/{slug}/users', forumSlugUsers),
                        web.get('/api/post/{slug}/details', postSlugDetailsGet),
                        web.get('/api/service/status', status),

                        web.post('/api/user/{slug}/create', userSlugCreate),
                        web.post('/api/user/{slug}/profile', userSlugProfile),
                        web.post('/api/forum/create', forumCreate),
                        web.post('/api/forum/{slug}/create', forumSlugCreate),
                        web.post('/api/forum/{slug}/threads', forumSlugThreads),
                        web.post('/api/thread/{slug}/create', threadSlugCreate),
                        web.post('/api/thread/{slug}/vote', threadSlugVote),
                        web.post('/api/thread/{slug}/details', threadSlugDetails),
                        web.post('/api/post/{slug}/details', postSlugDetails),
                        web.post('/api/service/clear', clear)
                    ])
    app['pool'] = await asyncpg.create_pool(**CONNECTION_DICT, min_size=10, max_size=10)
    app['user_profile_pool'] = await asyncpg.create_pool(**CONNECTION_DICT, init=init_user_profile_pool, min_size=10, max_size=10)
    app['forum_details_pool'] = await asyncpg.create_pool(**CONNECTION_DICT, init=init_forum_details_pool, min_size=10, max_size=10)
    app['post_details_pool'] = await asyncpg.create_pool(**CONNECTION_DICT, init=init_post_details_pool, min_size=10, max_size=10)
    return app

# app.make_handler(access_log=None)

# from gevent import queue
# from gevent.pywsgi import WSGIServer
if __name__ == "__main__":
    # loop.close()
    # import aiohttp_debugtoolbar
    # aiohttp_debugtoolbar.setup(app, max_visible_requests=100)
    web.run_app(my_web_app(), host='0.0.0.0', port=5000)
    # http = WSGIServer(('', 5000), app)
    # http.serve_forever()
