#!/usr/bin/env bash

service postgresql start

source venv/bin/activate

python migrate.py

# gunicorn --config gunicorn_config.py start_server:app
# python sanic_app.py
# gunicorn --config gunicorn_config.py aiohttp_app:app
python aiohttp_app.py