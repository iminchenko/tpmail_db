CONNECTION_DICT = {
    'user': 'postgres',
    'password': 'password01',
    'database': 'func_test',
    'host': 'localhost',
    'port': '5432',
}

CREATE_USERS = """
    CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        nickname varchar(64),
        email varchar(64),
        about varchar(1000),
        fullname varchar(100),
        nick_ordering varchar(64)
    );
    """
CREATE_FORUMS = """
    CREATE TABLE forums (
        id SERIAL PRIMARY KEY,
        user_id integer,
        name varchar(100),
        title varchar(100),
        user_nickname varchar(64),
        posts_count integer,
        threads_count integer
    );
    """

CREATE_THREADS = """
    CREATE TABLE threads (
        id SERIAL PRIMARY KEY,
        user_id integer,
        forum_id integer,
        created varchar(100),
        title varchar(100),
        message varchar(2000),
        slug varchar(100),
        user_nickname varchar(64),
        forum_name varchar(100),
        vote_value integer
    );
    """

CREATE_POSTS = """
    CREATE TABLE posts (
        id SERIAL PRIMARY KEY,
        user_id integer,
        forum_id integer,
        thread_id integer,
        parent_id integer,
        message varchar(2000),
        created varchar(100),
        isedited boolean,
        user_nickname varchar(64),
        forum_name varchar(100),
        user_email varchar(64),
        user_about varchar(1000),
        user_fullname varchar(100),
        tree_parent_path integer[],
        tree_path integer[],
        root integer
    );
    """

CREATE_VOTES = """
    CREATE TABLE votes (
        id SERIAL PRIMARY KEY,
        user_id integer,
        thread_id integer,
        voice integer
    );
    """

CREATE_USERFORUMS = """
    CREATE TABLE userforum (
        id SERIAL PRIMARY KEY,
        forum_id integer,
        nickname varchar(64),
        email varchar(64),
        about varchar(1000),
        fullname varchar(100),
        nick_ordering varchar(64)
    )
"""

CREATE_INDEXES = [
    "CREATE UNIQUE INDEX user_nickname_idx ON users (LOWER(nickname));",
    "CREATE UNIQUE INDEX user_email_idx ON users (LOWER(email));",
    "CREATE UNIQUE INDEX forum_name_idx ON forums (LOWER(name));",
    # "CREATE INDEX thread_created_idx ON threads (created);",
    "CREATE UNIQUE INDEX thread_slug_idx ON threads (LOWER(slug));",
    "CREATE INDEX thread_forum_idx ON threads (forum_id);",
    "CREATE INDEX post_thread_idx ON posts (thread_id);",
    "CREATE INDEX post_parent_idx ON posts (parent_id);",
    "CREATE INDEX post_forum_idx ON posts (forum_id);",
    "CREATE INDEX post_parentthread_idx ON posts (parent_id, thread_id);",
    "CREATE INDEX post_threadparent_idx ON posts (thread_id, parent_id);",
    "CREATE INDEX vote_thread_idx ON votes (thread_id);",
    "CREATE UNIQUE INDEX vote_userthread_idx ON votes (user_id, thread_id);",
    "CREATE UNIQUE INDEX vote_threaduser_idx ON votes (thread_id, user_id);",
    # "CREATE INDEX userforum_forum_id_idx ON userforum (forum_id);",
    "CREATE UNIQUE INDEX userforum_forum_id_user_nickname_idx ON userforum (forum_id, LOWER(nickname));"
    # "CREATE UNIQUE INDEX forum_id_user_nickname_idx ON userforum (forum_id, nickname)"
]

CREATE_RULES = [
    # """
    # CREATE OR REPLACE RULE posts_ins AS ON INSERT TO posts
    # DO ALSO
    # UPDATE posts SET 
    #     tree_path=array_append(NEW.tree_parent_path, NEW.id)
    # WHERE id = NEW.id;
    # """,

    """
    CREATE OR REPLACE FUNCTION myRule() RETURNS trigger as $myRule$
    BEGIN
        NEW.tree_path := array_append(NEW.tree_parent_path, NEW.id);
        RETURN NEW;
    END;
    $myRule$
    LANGUAGE plpgsql;
    """,

    """
    CREATE TRIGGER myRule 
    BEFORE INSERT 
    ON "posts"
    FOR EACH ROW 
    EXECUTE PROCEDURE myRule();
    """
]