# from app import *
from tables_creation import *
import asyncio
import asyncpg

async def migrate():
    # try:
    #     db.engine.execute("DROP SCHEMA public CASCADE;")
    # except:
    #     pass
    # db.engine.execute("CREATE SCHEMA public;")

    # db.engine.execute(CREATE_USERS)
    # db.engine.execute(CREATE_FORUMS)
    # db.engine.execute(CREATE_THREADS)
    # db.engine.execute(CREATE_POSTS)
    # db.engine.execute(CREATE_VOTES)

    # for i in CREATE_POSTS:
    #     db.engine.execute(i)
    conn = await asyncpg.connect(**CONNECTION_DICT)

    try:
        await conn.execute("DROP SCHEMA public CASCADE;")
    except:
        pass
    await conn.execute("CREATE SCHEMA public;")

    await conn.execute(CREATE_USERS)
    await conn.execute(CREATE_FORUMS)
    await conn.execute(CREATE_THREADS)
    await conn.execute(CREATE_POSTS)
    await conn.execute(CREATE_VOTES)
    await conn.execute(CREATE_USERFORUMS)

    for i in CREATE_INDEXES:
        await conn.execute(i)

    for i in CREATE_RULES:
        await conn.execute(i)

    await conn.close()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([asyncio.ensure_future(migrate())]))
    loop.close()