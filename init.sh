#!/usr/bin/env bash

sudo docker build -t test .

sudo docker run --memory 1G --log-opt max-size=1M --log-opt max-file=3 --net=host test