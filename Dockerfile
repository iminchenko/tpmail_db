FROM ubuntu:16.04

MAINTAINER Minchenko Ilia

RUN apt-get update -y
RUN apt-get install software-properties-common python-software-properties -y 
RUN add-apt-repository ppa:jonathonf/python-3.6 -y
RUN apt-get update -y
RUN apt-get install -y git
# RUN apt-get install libevent-2.0-5
# RUN apt-get install -y wget
RUN apt-get install -y python3.6
RUN apt-get install -y build-essential
RUN apt-get install -y python3-pip python3.6-dev virtualenv
# RUN apt-get install -y libpq-dev lbzip2

# Postgres
ENV PGVER 9.5
RUN apt-get install -y postgresql-$PGVER

USER postgres
RUN /etc/init.d/postgresql start &&\
    psql --command "ALTER USER postgres WITH SUPERUSER PASSWORD 'password01';" &&\
    createdb -E utf8 -T template0 -O postgres func_test &&\
    /etc/init.d/postgresql stop


RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/$PGVER/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/$PGVER/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "synchronous_commit = off" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "fsync = off" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/$PGVER/main/postgresql.conf

RUN echo "shared_buffers = 512MB" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "work_mem = 64MB" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "maintenance_work_mem = 128MB" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "effective_cache_size = 1GB" >> /etc/postgresql/$PGVER/main/postgresql.conf
RUN echo "max_wal_size = 1GB" >> /etc/postgresql/$PGVER/main/postgresql.conf
# RUN echo "cpu_tuple_cost = 0.0030" >> /etc/postgresql/$PGVER/main/postgresql.conf
# RUN echo "cpu_index_tuple_cost = 0.0010" >> /etc/postgresql/$PGVER/main/postgresql.conf
# RUN echo "cpu_operator_cost = 0.0005" >> /etc/postgresql/$PGVER/main/postgresql.conf

EXPOSE 5432

VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Flask
USER root

RUN git clone https://iminchenko@bitbucket.org/iminchenko/tpmail_db.git
ADD . /tpmail_db
WORKDIR /tpmail_db

# RUN wget https://bitbucket.org/pypy/pypy/downloads/pypy3-v6.0.0-linux64.tar.bz2
# RUN tar -x -C /opt -f pypy3-v6.0.0-linux64.tar.bz2
# RUN  mv /opt/pypy3-v6.0.0-linux64 /opt/pypy3
# RUN ln -s /opt/pypy3/bin/pypy3 /usr/local/bin/pypy3

RUN chmod +x ./venv.sh
RUN ./venv.sh

EXPOSE 5000

# Running

USER root

RUN chmod +x ./run.sh

ENTRYPOINT ["./run.sh"]
